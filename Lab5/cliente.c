//gcc -Wall cliente.c -o cliente -lpthread

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>

#define SIZE_MSG 100
#define SIZE_BUFFER 10

int main() {
	int shmid;
	key_t key;
	char *mensajes, *s;
	sem_t *sem_cont, *sem_free;

	if((sem_cont = sem_open("/sem_cont", O_CREAT, 0644, 0)) == (sem_t *)-1) {
	perror("Error creando semaforo 1");
	}

	if((sem_free = sem_open("/sem_free", O_CREAT, 0644, SIZE_BUFFER)) == (sem_t *)-1) {
	perror("Error creando semaforo 2");
  	}

   /*Se requiere el segmento llamado "1234" creado por el servidor */
   key = 1234;

   /* Ubica el segmento */
   if ((shmid = shmget(key, SIZE_MSG*SIZE_BUFFER, 0666)) < 0) {
       perror("shmget");
       exit(1);
   }

   /* Se adhiere al segmento para poder hacer uso de él */
   if ((mensajes = shmat(shmid, NULL, 0)) == (char *) -1) {
       perror("shmat");
       exit(1);
   }

   /* Lee lo que el servidor puso en la memoria */
   s = mensajes;
   int i = 0;
   char substring[100];
   while(1){
     sem_wait(sem_cont);
		 if(!(s[(i*SIZE_MSG + 99) % SIZE_MSG*SIZE_BUFFER])){
			 memcpy(substring, s, 100);
	     printf("%s", substring);
			 s += (SIZE_MSG - 1) % SIZE_MSG*SIZE_BUFFER;
       *s = 1;
       s++;
		 }else{
			 s += SIZE_MSG % SIZE_MSG*SIZE_BUFFER;
		 }
     sem_post(sem_free);
     i++;
   }
	 sem_close(sem_cont);
   sem_close(sem_free);
   sem_unlink("/sem_cont");
   sem_unlink("/sem_free");
   exit(0);
}
