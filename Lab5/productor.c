//gcc -Wall productor.c -o productor -lpthread

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>

#define SIZE_MSG 100
#define SIZE_BUFFER 10

int main(){
  int shmid;
  key_t key;
  char * mensajes, *s;
  sem_t *sem_cont, *sem_free;

  if((sem_cont = sem_open("/sem_cont", O_CREAT, 0644, 0)) == (sem_t *)-1) {
   perror("Error creando semaforo 1");
  }

  if((sem_free = sem_open("/sem_free", O_CREAT, 0644, SIZE_BUFFER)) == (sem_t *)-1) {
   perror("Error creando semaforo 2");
  }


  /*Nombre del segmento de memoria compartida = "1234".*/
  key = 1234;

  /* Se crea el segmento de memoria*/
  if ((shmid = shmget(key, SIZE_MSG*SIZE_BUFFER, IPC_CREAT | 0666)) < 0) {
   perror("shmget");
   exit(1);
  }

  /*El programa se adhiere (attach) al segmento ya creado */
  if ((mensajes = shmat(shmid, NULL, 0)) == (char *) -1) {
   perror("shmat");
   exit(1);
  }

  /* Se ponen algunos datos en el segmento para que el proceso cliente los lea */
  s = mensajes;
  s += 99;
  for(int i = 0; i < SIZE_BUFFER; i++){
    *s = 1;
    s += SIZE_MSG;
  }
  int i = 0;
  s = mensajes;
  while(1){
    sem_wait(sem_free);
    if(s[(i*SIZE_MSG + 99) % SIZE_MSG*SIZE_BUFFER]){
      printf("Escriba: ");
      fgets(s, 100, stdin);
      if(!strcmp(s, "exit\n")){
        break;
      }
      s += (SIZE_MSG - 1) % SIZE_MSG*SIZE_BUFFER;
      *s = 0;
      s++;
    }else{
      s += SIZE_MSG % SIZE_MSG*SIZE_BUFFER;
    }
    sem_post(sem_cont);
    i++;
  }
  s = NULL;
  sem_close(sem_cont);
  sem_close(sem_free);
  sem_unlink("/sem_cont");
  sem_unlink("/sem_free");

  if (shmctl(shmid, IPC_RMID, 0) == -1)
    fprintf(stderr, "shmctl(IPC_RMID) error\n");
  else{
     printf(" Memoria liberada ");
     exit(0);
   }

  return(0);
}
