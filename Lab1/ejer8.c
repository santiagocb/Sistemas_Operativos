#include <stdio.h>
#include <string.h>

int main(){
  char subject[3][50];
  char level[3][50];
  char flag[50], flag2[50];
  int i = 0;
  printf("Ingrese el nombre de tres materias con su respectiva dificultad (Bajo, Medio, Alto)\n");
  printf("%d\n", (sizeof(subject)/sizeof(subject[0])));
  while (i < 3) {
    printf("Nombre de la materia #%d: ", i + 1);
    scanf("%s", flag);
    strcpy(subject[i], flag);
    printf("Su nivel de dificultad: ");
    scanf("%s",flag2);
    strcpy(level[i], flag2);
    i++;
  }
  i = 0;
  printf("|%-25s|", "Curso");
  printf("%-25s|\n", "Dificultad");
  while (i < 3) {
    printf("|%25s|", subject[i]);
    printf("%25s|\n", level[i]);
    i++;
  }

  return 0;
}
