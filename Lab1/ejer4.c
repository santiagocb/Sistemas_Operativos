

#include <stdio.h>
#include <string.h>

int main()
{
    int edad;
    char clasificacion[40];
    char enun[] = "Clasificación por edad";
    char enun2[] = "Edad en meses";
    char ustedes[] = "Usted es un ";
    char buff[20];
    printf("Ingrese su edad en años: ");
    scanf("%d", &edad);
    printf("|%-25s|", enun);
    printf("%-25s|\n", enun2);

    if(0 <= edad && edad <= 2){
      strcpy(clasificacion, "Bebé");
    }
    if(3 <= edad && edad <= 12){
      strcpy(clasificacion, "Niño");
    }
    if(13 <= edad && edad <= 17){
      strcpy(clasificacion, "Adolescente");
    }
    if(edad >= 18){
      strcpy(clasificacion, "Adulto");
    }
    printf("|%25s|", strcat(ustedes, clasificacion));
    sprintf(buff, "%d meses", edad * 12);              //put string into buff
    printf("%25s|\n", buff);
    return 0;
}
