#include <stdio.h>
#include <string.h>

int isVowel(char);

int main(){
  char word[30];
  char first[1];
  printf("Type a word: ");
  scanf("%s", word);
  strncpy(first, word, 1);
  if(isVowel(first[0])){
    printf("The word %s begins by vowel %c\n", word, first[0]);
  }else{
    printf("The word %s doesn't begin by a vowel\n", word);
  }
  return 0;
}

int isVowel(char ch){
  int isLowerCase, isUpperCase;
  if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'){
    isLowerCase = 1;
  }else{
    isLowerCase = 0;
  }
  if(ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U'){
    isUpperCase = 1;
  }else{
    isUpperCase = 0;
  }
  if(isLowerCase || isUpperCase){
    return 1;
  }else{
    return 0;
  }
}
