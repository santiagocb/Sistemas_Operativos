
#include <stdio.h>
#include <string.h>

int main(){
  int number;
  char out[20];
  printf("Type a number: ");
  scanf("%d", &number);
  if (number % 2 == 0) {
    sprintf(out, "Number %d is even", number);
  }else{
    sprintf(out, "Number %d is odd", number);
  }
  printf("%s\n", out);
  return 0;
}
