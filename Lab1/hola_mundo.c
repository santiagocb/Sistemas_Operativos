/* Programa Hola Mundo
 * Esto es un comentario de varias líneas
 * debe empezar con los caracteres (slash)(asterisco) y
 * finalizar con (asterisco)(slash)
 */

#include <stdio.h>

int main()
{
    // Comentario de una sola línea
    printf( "Hola mundo.\n" );
    return 0;
}
