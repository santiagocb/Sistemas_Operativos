//Evaluación laboratorio #1 Sistemas Operativos 2017-2
//Autor: Santiago Cadavid Bustamante

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int imprimir_menu();
int potencia2(int);
int dec2bin(int);
int dec2oct(int);
int dec2base(int, int);

int main(){
  int opcion, numero, result, base;
  do{
    opcion = imprimir_menu();
    switch (opcion) {
      case 1:
        printf("Digite la potencia: ");
        scanf("%d", &numero);
        result = potencia2(numero);
        printf("\tResultado: %d\n", result);
        break;
      case 2:
        printf("\tDigite el entero: " );
        scanf("%d", &numero);
        result = dec2bin(numero);
        printf("\tResultado: %d(2)\n", result);
        break;
      case 3:
        printf("\tDigite el entero: " );
        scanf("%d", &numero);
        result = dec2oct(numero);
        printf("\tResultado: %d(8)\n", result);
        break;
      case 4:
        printf("\tSaliendo del programa..\n");
        exit(0);
        break;
      case 5:
        printf("\tDigite el entero: " );
        scanf("%d", &numero);
        printf("\tDigite la base: " );
        scanf("%d", &base);
        result = dec2base(numero, base);
        printf("\tResultado: %d(%d)\n", result, base);
        break;
      default:
        if(isalpha(opcion)){
          printf("\tNo se permiten letras\n");    //profe no me dio al final :/
        }
        printf("\tOpción denegada..\n");
        break;
    }
  } while(opcion != 4);
  return 0;
}

int imprimir_menu(){
  int n;
  printf("%s\n", "Ingrese la actividad a desarrollar: ");
  printf("\t 1) Calcular la potencia de 2^n.\n" );
  printf("\t 2) Convertir número en sistema decimal a Binario.\n" );
  printf("\t 3) Convertir número de sistema decimal a Octal.\n" );
  printf("\t 4) Salir del programa.\n" );
  printf("\t 5) Convertir número de sistema decimal a cualquiera base.\n" );
  printf("Digite la opcion: ");
  scanf("%d", &n);
  return n;
}

int potencia2(int n){
  int resultado = 1;
  int i = 0;
  while (i < n){
    resultado *= 2;
    i++;
  }
  return resultado;
}

int dec2bin(int dec){
  int resul = 0;
  int parasum = 1;
  while(dec != 0){
    resul += (dec % 2)*parasum;
    parasum *= 10;
    dec = dec / 2;
  }
  //resul += (dec % 2)*parasum;
  return resul;
}

int dec2oct(int dec){
  int resul = 0;
  int parasum = 1;
  while(dec != 0){
    resul += (dec % 8)*parasum;
    parasum *= 10;
    dec = dec / 8;
  }
  return resul;
}

int dec2base(int dec, int base){
  int resul = 0;
  int parasum = 1;
  while(dec != 0){
    resul += (dec % base)*parasum;
    parasum *= 10;
    dec = dec / base;
  }
  return resul;
}
