#include <stdio.h>

int main(){
  int i, measure;
  int array[5];
  for(i = 1; i <= 5; i++){
    printf("Type the #%d patient's height in cm: ", i);
    scanf("%d", &measure);
    array[i - 1] = measure;
  }
  for(i = 1; i <= 5; i++){
    printf("The patient #%d measures %d\n", i, array[i - 1]);
  }
  return 0;
}
