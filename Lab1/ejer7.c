#include <stdio.h>

int main(){
  char name[24];
  enum list_status {
    restringido,
    lectura,
    lecturayescritura,
  } status;
  printf("Nombre de un archivo: ");
  scanf("%s", name);
  printf("Tipo de permiso: ");
  scanf("%d", &status);
  switch (status) {
    case 0:
      printf("El archivo %s tiene permiso de acceso restringido\n", name);
      break;
    case 1:
      printf("El archivo %s tiene permiso de acceso lectura\n", name);
      break;
    case 2:
      printf("El archivo %s tiene permiso de acceso lectura y escritura\n", name);
      break;
    default:
      printf("El tipo de permiso %d no existe", status);
      break;
  }
  return 0;
}
