
#include <stdio.h>

int min(int*, int);
int max(int*, int);
float average(int*, int);

int main()
{
    int arreglo[5] = {1, 2, 3, 4, 5};
    /*int count = 0;
    char entry;
    scanf("")
    while(count < 10 || entry != 'x'){

    }*/
    printf("The average of array is: %f\n", average(arreglo, 5));
    printf("The minium of array is: %d\n", min(arreglo, 5));
    printf("The maximum of array is: %d\n", max(arreglo, 5));

    //aun falta implementar el min y el max

    return 0;
}

float average(int* a, int tamano){
  int i;
  float average = 0;
  for(i = 0; i < tamano; i++){
    average = average + a[i];
  }
  return average/tamano;
}

int min(int* a, int tamano){
  int i;
  int min = a[0];
  for(i = 0; i < tamano; i++){
    if(a[i] < min){
      min = a[i];
    }
  }
  return min;
}

int max(int* a, int tamano){
  int i;
  int max = a[0];
  for(i = 0; i < tamano; i++){
    if(a[i] > max){
      max = a[i];
    }
  }
  return max;
}
