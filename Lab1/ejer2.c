
#include <stdio.h>
#include <math.h>       // -lm en compilador para que lea esta librería

float area(float, float);
float perimeter(float, float);
float facil(double);

int main(){
  float height;
  float base;
  printf("\033[36;1mConsider right-angled triangle \033[0m\n");
  printf("Type triangule's base: ");
  scanf("%f", &base);
  printf("Type triangule's height: ");
  scanf("%f", &height);
  printf("Area: %.2f\n", area(base, height));
  printf("Perimeter: %.2f\n",  perimeter(base, height));
  return 0;
}

float area(float base, float height){
  return base * height / 2;
}

float perimeter(float base, float height){
  return sqrt(pow(base, 2) + pow(height, 2));
}
