//Evaluación laboratorio #2 Sistemas Operativos 2017-2
//Autor: Santiago Cadavid Bustamante
//Compilar: gcc -Wall eval2_CadavidSantiago.c -o eval2_CadavidSantiago
//Ejecutar: ./eval2_CadavidSantiago

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

typedef struct proc_{
  int id;
  char name[40];
  int size;
} PROC;

void print_menu(int *);  //También se modifica la firma
int load_procs(FILE* procf_desc, PROC** p_procs);
void print_procs(PROC* p_procs, int n_entries);
int create_proc(PROC**, int*);
void save_procs(PROC* p_procs, int n_entries, FILE* procf_desc);
int delete_proc(PROC**, int*);

char* proc_file_name = "proc_file";
int main(){
  int option;
  int num_process;
  PROC* process_ptr;
  FILE *proc_file = fopen(proc_file_name, "r");
  if(proc_file == NULL){
    printf("\t\033[31;1mNo se pudo abrir el fichero %s\033[0m\n", proc_file_name);
    exit(1);
  }else{
    num_process = load_procs(proc_file, &process_ptr);
  }
  do{
    print_menu(&option);
    switch (option) {
      case 1:
        print_procs(process_ptr, num_process);
        break;
      case 2:
        if(create_proc(&process_ptr, &num_process)){
          printf("\t\033[32;1mProceso creado exitosamente\033[0m\n");
        }else{
          printf("\t\033[31;1mEl proceso con ese ID ya existe\033[0m\n");
        }
        break;
      case 3:
        if(!delete_proc(&process_ptr, &num_process)){
          printf("\t\033[31;1mNo existe un proceso con ese id\033[0m\n");
        }else{
          printf("\t\033[32;1mProceso borrado exitosamente\033[0m\n");
        }
        break;
      case 4:
        fclose(proc_file);
        proc_file = fopen(proc_file_name, "w");
        save_procs(process_ptr, num_process, proc_file);
        printf("Los cambios se guardaron exitosamente\n");
        break;
      case 5:
        printf("No se guardarán los cambios\n");
        break;
      default:
        printf("\t\033[31;1mOpción no válida\033[0m\n");
        break;
    }
  }while(option != 4 && option != 5);
  printf("Saliendo del programa..\n");
  fclose(proc_file);
  free(process_ptr);
  return 0;
}

void print_menu(int *option){      //Se modificó el parámetro del método, ya que se manejará como referencia
  *option = 0;
  printf("\033[33;1mUdeA-OS Menu\033[0m\n");
  printf("1) Mostrar información actual del archivo \n");
  printf("2) Crear una nueva entrada de proceso (en memoria dinámica) \n");
  printf("3) Eliminar una entrada del proceso (en memoria dinámica) \n");
  printf("4) Salir guardando cambios \n");
  printf("5) Salir sin guardar cambios \n");
  printf("Digite la opción a realizar: ");
  scanf("%d", option);
}

void save_procs(PROC* p_procs, int n_entries, FILE* procf_desc){
  for(int i = 0; i < n_entries; i++){
    PROC structure = (PROC)p_procs[i];
    fprintf(procf_desc, "%d\t%10s\t%d\n", structure.id, structure.name, structure.size);
  }
}

int create_proc(PROC** p_procs, int* n_entries){
  int pid;
  if(p_procs == NULL){
    printf("\tError en la reserva del espacio\n");
    exit(1);
  }
  printf("\tDigite datos acerca del proceso..\n");
  printf("\tID: ");
  scanf("%d", &pid);
  for(int i = 0; i < *n_entries; i++){
    if((*p_procs)[i].id == pid){
      return 0;
    }
  }
  *n_entries += 1;
  *p_procs = (PROC *) realloc(*p_procs, sizeof(PROC)*(*n_entries));
  (*p_procs)[*n_entries - 1].id = pid;
  printf("\tNombre: ");
  scanf("%s", (*p_procs)[*n_entries - 1].name);
  printf("\tTamano: ");
  scanf("%d", &(*p_procs)[*n_entries - 1].size);
  return 1;
}

int delete_proc(PROC** p_procs, int* n_entries){
  int pid;
  printf("Procesos en ejecución \n");
  print_procs(*p_procs, *n_entries);
  printf("\tDigite el ID del proceso a borrar: ");
  scanf("%d", &pid);
  for(int i = 0; i < *n_entries; i++){
    if((*p_procs)[i].id == pid){
      for(int j = i; j < *n_entries - 1; j++){
        (*p_procs)[j] = (*p_procs)[j + 1];
      }
      *n_entries -= 1;
      *p_procs = (PROC *) realloc (*p_procs, sizeof(PROC)*(*n_entries));
      return 1;
    }
  }
  return 0;
}


int load_procs(FILE* procf_desc, PROC** p_procs){
  int entries;
  *p_procs = NULL;
  entries = 0;
  while(feof(procf_desc) == 0){
    entries++;
    *p_procs = (PROC *) realloc(*p_procs, sizeof(PROC)* entries);
    if(p_procs == NULL){
      printf("\t\033[31;1mError en la reserva del espacio\033[0m\n");
      exit(1);
    }
    fscanf(procf_desc, "%d\t%10s\t%d\t", &(*p_procs)[entries - 1].id,
          (*p_procs)[entries - 1].name, &(*p_procs)[entries - 1].size);
  }
  return entries;
}

void print_procs(PROC* p_procs, int n_entries){
  printf("\t %s\t%10s\t%s\n", "id", "name", "size");
  for(int i = 0; i < n_entries; i++){
    printf("\t %d\t%10s\t%d\n", p_procs[i].id, p_procs[i].name, p_procs[i].size);
  }
}
