/*  Autor: Danny Munera
    Editor: Santiago Cadavid Bustamante
    Seguimiento 2do laboratorio
    Sistemas Operativos
*/
#include<stdio.h>
#include<stdlib.h>

/* This program can create, multiply and print 2D-matrices*/

/* Functions to provide */
int** create_matrix(int order);
void print_matrix(int** matrix, int order);
void free_matrix(int**, int order);
int** mult_matrix(int** m_A, int** m_B, int order);

int main(int argc, char *argv[]){

  int** matrix_A;
  int** matrix_B;
  int** matrix_R;
  int order;

  if( argc != 2 ){
    printf("ERROR: you need to provide the square matrices order\n");
    return -1;
  }
  else{
    printf("The matrices order is %s x %s\n",argv[1],argv[1]);
  }

  order = atoi(argv[1]);

  printf("Creating Matrix A:\n");
    matrix_A = create_matrix(order);
    print_matrix(matrix_A, order);

  printf("Creating Matrix B:\n");
    matrix_B = create_matrix(order);
    print_matrix(matrix_B, order);

  printf("Multiplying Matrices A * B:\n");
    matrix_R = mult_matrix(matrix_A, matrix_B, order);
    print_matrix(matrix_R, order);

  printf("freeing memory...\n");
    free_matrix(matrix_A, order);
    free_matrix(matrix_B, order);
    free_matrix(matrix_R, order);
  return 0;
}

int** create_matrix(int order){
  int** p_m = (int**)malloc(sizeof(int*)*order);
  if(p_m == NULL){
    printf("Error while reserving memory for matrix");
    exit(1);
  }
  for(int i = 0; i < order; i++){
    p_m[i] = (int*)malloc(sizeof(int)*order);
    if(p_m[i] == NULL){
      printf("Error while reserving memory for matrix");
      exit(1);
    }
  }
  for(int i = 0; i < order; i++){
    int* pointer = p_m[i];
    for(int j = 0; j < order; j++){
      pointer[j] = rand() % 10;
      //p_m[i][j] = rand() % 50;
    }
  }
  printf("In create_matrix\n");
  return p_m;
}

void print_matrix(int** matrix, int order){
  for(int i = 0; i < order; i++){
    for (int j = 0; j < order; j++) {
      printf("%d\t", matrix[i][j]);
    }
    printf("\n");
  }
  printf("In print_matrix\n");
}

void free_matrix(int** matrix, int order){
  for (int i = 0; i < order; i++) {
    int* pointer = matrix[i];
    free(pointer);
  }
  free(matrix);
  printf("In free_matrix\n");
}

int** mult_matrix(int** m_A, int** m_B, int order){
  int** m_R = (int**)malloc(sizeof(int*)*order);
  if(m_R == NULL){
    printf("Error while reserving memory for matrix");
    exit(1);
  }
  for(int i = 0; i < order; i++){
    m_R[i] = (int*)malloc(sizeof(int)*order);
    if(m_R[i] == NULL){
      printf("Error while reserving memory for matrix");
      exit(1);
    }
  }
  int resul = 0;
  for(int i = 0; i < order; i++){
    for(int j = 0; j < order; j++){
      for (int k = 0; k < order; k++) {
        resul += m_A[i][k] * m_B[k][j];
      }
      m_R[i][j] = resul;
      resul = 0;
    }
  }
  printf("In mult_matrix\n");
  return m_R;
}
