#include <stdio.h>
#define SIZE 100

int main(void)
{
   printf("Color %s, Number %d, Float %5,2f", "red", 123456, 3.14);
   //printf("\r");
   printf("%d", 'A');
   printf("\007 That was a beep\n");
   return 0;
}
