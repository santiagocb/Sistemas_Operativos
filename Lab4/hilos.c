//Compilar: gcc -Wall hilos.c -o hilos -lpthread -lm
//Compilar optimizado: gcc -Wall hilos.c -o hilos -lpthread -lm -march=native -O2

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>

#define RED     "\x1b[31m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define GREEN   "\x1b[32m"
#define RESET   "\x1b[0m"

struct t_param{
  int* vector1;
  int* vector2;
  int inicial;
  int end;
};

void * multiply(void *);
int vectorSize(char *);
void fillVector(int *, FILE *);
int createThreads(int, int, int*, int*);

int main(int argc, char *argv[]){
  struct timeval t0, t1, t2, t3, dt, dtp;
  gettimeofday(&t2, NULL);
  if(argc != 4){
    printf(RED "Hacen falta más parámetros\n"RESET);
    return 1;
  }
  char vector1[100], vector2[100], benchmark[50];
  int * ptr_vector1, * ptr_vector2;
  FILE * file1, * file2;
  int size;

  strcpy(vector1, argv[1]);
  strcpy(vector2, argv[2]);
  int n_threads = atoi(argv[3]);

  strcpy(benchmark, "benchmark/");
  strcat(benchmark, vector1);
  file1 = fopen(benchmark, "r");

  strcpy(benchmark, "benchmark/");
  strcat(benchmark, vector2);
  file2 = fopen(benchmark, "r");

  if(file1 == NULL || file2 == NULL){
    printf(RED"\tNo se pudo abrir alguno de los dos ficheros\n"RESET);
    exit(1);
  }

  if(vectorSize(vector1) != vectorSize(vector2)){
    printf(RED"\tTamaños de vectores diferentes\n"RESET);
    return 1;
  }
  size = vectorSize(vector1);
  ptr_vector1 = malloc(sizeof(int)*size);
  ptr_vector2 = malloc(sizeof(int)*size);

  if(ptr_vector1 == NULL || ptr_vector2 == NULL){
    printf(RED"\tNo fue posible asignar espacio en memoria\n"RESET);
    return 1;
  }

  fillVector(ptr_vector1, file1);
  fillVector(ptr_vector2, file2);
  gettimeofday(&t0, NULL);
  printf("Producto punto: %d\n", createThreads(n_threads, vectorSize(vector1), ptr_vector1, ptr_vector2));
  gettimeofday(&t1, NULL);
  timersub(&t1, &t0, &dt);
  printf(MAGENTA"La ejecución desde el método create, tarda: %ld.%06ld s\n"RESET, dt.tv_sec, dt.tv_usec);
  free(ptr_vector1);
  free(ptr_vector2);
  gettimeofday(&t3, NULL);
  timersub(&t3, &t2, &dtp);
  printf(GREEN"La ejecución de todo el programa, tarda: %ld.%06ld s\n"RESET, dtp.tv_sec, dtp.tv_usec);
  return 0;
}

int vectorSize(char * vectorName){
  int base, exponent;
  char substringBase[100], substringExp[100];
  memcpy(substringBase, &vectorName[4], 2);      //Obteniendo substring para la base.
  memcpy(substringExp, &vectorName[7], 1);       //Obteniendo substring para la la potencia.
  base = atoi(substringBase);
  exponent = atoi(substringExp);
  return pow(base, exponent);
}

void fillVector(int * ptr_vector, FILE * ptr_file){
  char buffer[10];
  int i = 0;

  while(fgets(buffer, sizeof(buffer), ptr_file)){
    ptr_vector[i] = atoi(buffer);
    i++;
  }
  fclose(ptr_file);
}

int createThreads (int n_threads, int vectorSize, int* vector1, int* vector2){

  if(n_threads > vectorSize){
    n_threads = vectorSize;
  }

  pthread_t threads_ids[n_threads];
  int iniPos, endPos, error, point = 0; //Posiciones iniciales y finales para cada hilo. Point = producto punto total.
  int toAssign = vectorSize; //Posiciones del vector que quedan por asignar.
  int divi = vectorSize/n_threads; //A cada hilo se le dan partes iguales del vector.
  int missPos = vectorSize%n_threads; //Si la división no es entera, quedan "posiciones perdidas".
  struct timeval t0, t1, dt;
  struct t_param param_thread[n_threads];

  for(int i= 0; i < n_threads; i++){
    iniPos = vectorSize - toAssign;
    toAssign = toAssign - divi;
    if(missPos != 0){     //Se le asigna más posiciones a los primeros hilos.
      endPos = vectorSize - toAssign;
      --toAssign;
      --missPos;
    }else{
      endPos = vectorSize - toAssign -1;
    }

    //Asignación de posiciones.
    param_thread[i].vector1 = vector1;
    param_thread[i].vector2 = vector2;
    param_thread[i].inicial = iniPos;
    param_thread[i].end = endPos;

  }
  gettimeofday(&t0, NULL);
  for (int j = 0; j < n_threads; j++){
    error = pthread_create(&threads_ids[j], NULL, (void*) multiply, &param_thread[j]);
    if(error != 0){
      printf(RED"\tError, el hilo no pudo ser creado\n"RESET);
      exit(1);
    }
  }
  for (int k = 0; k < n_threads; k++){
    int * value;
    pthread_join(threads_ids[k], (void**)&value);
    point += *value;
    free(value);
  }
  gettimeofday(&t1, NULL);
  timersub(&t1, &t0, &dt);
  printf(BLUE"La ejecución desde la creación del hilo tarda: %ld.%06ld s\n"RESET, dt.tv_sec, dt.tv_usec);
  return point;
}

void* multiply(void* arg){
  struct t_param* args = (struct t_param*) arg;
  int * result = (int*) malloc(sizeof(int));
  int register aux = 0;

  for (int k = (*args).inicial; k <= (*args).end; k++){
    aux += (*args).vector1[k] * (*args).vector2[k];
  }
  *result = aux;
  return result;
}
