//Compilar: gcc -Wall porhilos.c -o porhilos -lpthread -lm

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

void multiply(int *, int *, int **, int);
int vectorSize(char *);
void fillVector(int **, FILE *);

int main(int argc, char *argv[]){
  if(argc != 3){
    printf("Hacen falta más parámetros\n");
    return 1;
  }
  char vector1[100], vector2[100], benchmark[50];
  int * ptr_vector1, * ptr_vector2, * result;
  FILE * file1, * file2;
  //int thread_num;

  strcpy(vector1, argv[1]);
  strcpy(vector2, argv[2]);
  //thread_num = atoi(argv[3]);

  strcpy(benchmark, "benchmark/");
  strcat(benchmark, vector1);
  file1 = fopen(benchmark, "r");

  strcpy(benchmark, "benchmark/");
  strcat(benchmark, vector2);
  file2 = fopen(benchmark, "r");

  if(file1 == NULL || file2 == NULL){
    printf("\t\033[31;1mNo se pudo abrir alguno de los dos ficheros\033[0m\n");
    exit(1);
  }

  if(vectorSize(vector1) != vectorSize(vector2)){
    printf("\t\033[31;1mTamaños de vectores diferentes\033[0m\n");
    return 1;
  }

  ptr_vector1 = malloc(sizeof(int)*vectorSize(vector1));
  ptr_vector2 = malloc(sizeof(int)*vectorSize(vector2));
  result = malloc(sizeof(int)*vectorSize(vector2));

  fillVector(&ptr_vector1, file1);
  fillVector(&ptr_vector2, file2);

  multiply(ptr_vector1, ptr_vector2, &result, vectorSize(vector1));

  free(ptr_vector1);
  free(ptr_vector2);
  free(result);
  return 0;
}

int vectorSize(char * vectorName){
  int base, exponent;
  char substringBase[100], substringExp[100];
  memcpy(substringBase, &vectorName[4], 2);      //Obteniendo substring para la base.
  memcpy(substringExp, &vectorName[7], 1);       //Obteniendo substring para la la potencia.
  base = atoi(substringBase);
  exponent = atoi(substringExp);
  return pow(base, exponent);
}

void fillVector(int ** ptr_vector, FILE * ptr_file){
  char buffer[10];
  int i = 0;

  while(fgets(buffer, sizeof(buffer), ptr_file)){
    (*ptr_vector)[i] = atoi(buffer);
    i++;
  }
  free(ptr_file);
}

void multiply(int * ptr_vector1, int * ptr_vector2, int ** result, int size){
  printf("| ");
  for(int i = 0; i < size; i++){
    (*result)[i] = ptr_vector1[i] * ptr_vector2[i];
    printf("%d |", (*result)[i]);
  }
  printf("\n");
}
