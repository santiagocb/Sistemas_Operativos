//Compilar: gcc -Wall clase09b.c -o clase09b -lpthread -lm

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

void * calcularSuma(void * arg);

struct parametros_hilo{
  int * vector;
  int inicio;
  int final;
  int resultado;
};

int main(){
  char * name_file = "vec_10_1_a.txt";
  FILE * ptr_file = fopen("benchmark/vec_10_1_a.txt", "r");//strcat("benchmark/", name_file)
  int * vector = NULL;
  int i = 1, base, pot;
  double tamano;
  char buffer[10], buffer2[100], buffer3[100];
  while(fgets(buffer, sizeof(buffer), ptr_file)){
    vector = (int *) realloc(vector, sizeof(int)*i);
    vector[i - 1] = atoi(buffer);
    i++;
  }
  memcpy(buffer2, &name_file[4], 2);
  memcpy(buffer3, &name_file[7], 1);
  base = atoi(buffer2);
  pot = atoi(buffer3);
  tamano = pow(base, pot);

  pthread_t id_hilo1;
  pthread_t id_hilo2;

  struct parametros_hilo parametros_hilo1;
  struct parametros_hilo parametros_hilo2;

  parametros_hilo1.vector = vector;
  parametros_hilo1.inicio = 0;
  parametros_hilo1.final = tamano/2 - 1;

  parametros_hilo2.vector = vector;
  parametros_hilo2.inicio = tamano/2;
  parametros_hilo2.final = tamano - 1;

  pthread_create(&id_hilo1, NULL, &calcularSuma, &parametros_hilo1);
  pthread_create(&id_hilo2, NULL, &calcularSuma, &parametros_hilo2);

  printf("Suma total del vector = %d\n", parametros_hilo1.resultado + parametros_hilo2.resultado);
  free(vector);
  return 0;
}

void * calcularSuma(void * arg){
  int suma = 0;
  struct parametros_hilo * args = (struct parametros_hilo *) arg;
  for(int i = (*args).inicio; i <= (*args).final; i++){
    suma += (*args).vector[i];
  }
  (*args).resultado = suma;
  return NULL;
}
