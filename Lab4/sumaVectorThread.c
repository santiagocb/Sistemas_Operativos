//Compilar: gcc -Wall sumaVectorThread.c -o sumaVectorThread -lpthread

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

struct parametros_hilo{
  int * vector;
  int inicio;
  int final;
};

void * calcularSuma(void * arg);

int main(){
  clock_t begin, end;
  //time_t start = time(NULL);
  double time_spent;
  FILE * ptr_file = fopen("vector.txt", "r");
  char buffer[10];
  int count = 0, i = 0;
  while(fgets(buffer, sizeof(buffer), ptr_file)){
    count++;
  }
  rewind(ptr_file);
  int vector[count];
  while(fgets(buffer, sizeof(buffer), ptr_file)){
    vector[i] = atoi(buffer);
    i++;
  }
  /*for(int j = 0; j < count; j++){
    printf("%d\n", vector[j]);
  }*/
  pthread_t id_hilo1;
  pthread_t id_hilo2;
  int suma1, suma2;

  begin = clock();
  struct parametros_hilo parametros_hilo1;
  struct parametros_hilo parametros_hilo2;

  parametros_hilo1.vector = vector;
  parametros_hilo1.inicio = 0;
  parametros_hilo1.final = count/2 - 1;

  parametros_hilo2.vector = vector;
  parametros_hilo2.inicio = count/2;
  parametros_hilo2.final = count - 1;

  pthread_create(&id_hilo1, NULL, &calcularSuma, &parametros_hilo1);
  pthread_create(&id_hilo2, NULL, &calcularSuma, &parametros_hilo2);

  pthread_join(id_hilo1, (void*) &suma1);
  pthread_join(id_hilo2, (void*) &suma2);

  printf("Suma total del vector = %d\n", suma1 + suma2);
  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
   printf("Elapsed time: %.21lf seconds.\n", time_spent);


   //printf("%.4f\n", (double)(time(NULL) - start));   otra forma
  return 0;
}

void * calcularSuma(void * arg){
  int suma = 0;
  struct parametros_hilo * args = (struct parametros_hilo *) arg;
  for(int i = (*args).inicio; i <= (*args).final; i++){
    suma += (*args).vector[i];
  }
  return (void*) suma;
}
