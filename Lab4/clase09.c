//Compilar: gcc -Wall clase09.c -o clase09 -lpthread -lm

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

int calcularSuma(int *, int);

int main(){
  char * name_file = "vec_10_1_a.txt";
  FILE * ptr_file = fopen("benchmark/vec_10_1_a.txt", "r");//strcat("benchmark/", name_file)
  int * vector = NULL;
  int i = 1, base, pot;
  double tamano;
  char buffer[10], buffer2[100], buffer3[100];

  memcpy(buffer2, &name_file[4], 2);      //Obteniendo substring para la base.
  memcpy(buffer3, &name_file[7], 1);      ////Obteniendo substring para la la potencia.
  base = atoi(buffer2);
  pot = atoi(buffer3);
  tamano = pow(base, pot);

  vector = malloc(sizeof(int)* tamano);

  while(fgets(buffer, sizeof(buffer), ptr_file)){
    vector[i - 1] = atoi(buffer);
    i++;
  }

  printf("La suma es: %d\n", calcularSuma(vector, (int)tamano));
  free(vector);
  return 0;
}

int calcularSuma(int * vector, int tam){
  int suma = 0;
  for(int i = 0; i < tam; i++){
    //printf("%d\n", vector[i]);
    suma += vector[i];
  }
  return suma;
}
