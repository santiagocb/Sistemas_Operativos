//Compilar optimizado: gcc -Wall secuencial.c -o secuencial -lpthread -lm
//Compilar optimizado: gcc -Wall secuencial.c -o secuencial -lpthread -lm -march=native -O2

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>

#define RED     "\x1b[31m"
#define BLUE    "\x1b[34m"
#define GREEN   "\x1b[32m"
#define RESET   "\x1b[0m"

int multiply(int *, int *, int);
int vectorSize(char *);
void fillVector(int *, FILE *);

int main(int argc, char *argv[]){
  struct timeval t0, t1, t2, t3, dt, dtp;
  gettimeofday(&t2, NULL);
  if(argc != 3){
    printf(RED "Hacen falta más parámetros\n"RESET);
    return 1;
  }
  char vector1[100], vector2[100], benchmark[50];
  int * ptr_vector1, * ptr_vector2;
  FILE * file1, * file2;

  strcpy(vector1, argv[1]);
  strcpy(vector2, argv[2]);

  strcpy(benchmark, "benchmark/");
  strcat(benchmark, vector1);
  file1 = fopen(benchmark, "r");

  strcpy(benchmark, "benchmark/");
  strcat(benchmark, vector2);
  file2 = fopen(benchmark, "r");

  if(file1 == NULL || file2 == NULL){
    printf(RED"\tNo se pudo abrir alguno de los dos ficheros\n"RESET);
    exit(1);
  }

  if(vectorSize(vector1) != vectorSize(vector2)){
    printf(RED"\tTamaños de vectores diferentes\n"RESET);
    return 1;
  }

  ptr_vector1 = malloc(sizeof(int)*vectorSize(vector1));
  ptr_vector2 = malloc(sizeof(int)*vectorSize(vector2));

  if(ptr_vector1 == NULL || ptr_vector2 == NULL){
    printf(RED"\tNo fue posible asignar espacio en memoria\n"RESET);
    return 1;
  }

  fillVector(ptr_vector1, file1);
  fillVector(ptr_vector2, file2);
  gettimeofday(&t0, NULL);
  int mult = multiply(ptr_vector1, ptr_vector2, vectorSize(vector1));
  gettimeofday(&t1, NULL);
  printf("Producto punto: %d\n", mult);
  timersub(&t1, &t0, &dt);
  printf(BLUE"La ejecución desde el método multiply tarda: %ld.%06ld s\n"RESET, dt.tv_sec, dt.tv_usec);
  free(ptr_vector1);
  free(ptr_vector2);
  gettimeofday(&t3, NULL);
  timersub(&t3, &t2, &dtp);
  printf(GREEN"La ejecución de todo el programa, tarda: %ld.%06ld s\n", dtp.tv_sec, dtp.tv_usec);
  return 0;
}

int vectorSize(char * vectorName){
  int base, exponent;
  char substringBase[100], substringExp[100];
  memcpy(substringBase, &vectorName[4], 2);      //Obteniendo substring para la base.
  memcpy(substringExp, &vectorName[7], 1);       //Obteniendo substring para la la potencia.
  base = atoi(substringBase);
  exponent = atoi(substringExp);
  return pow(base, exponent);
}

void fillVector(int * ptr_vector, FILE * ptr_file){
  char buffer[10];
  int i = 0;

  while(fgets(buffer, sizeof(buffer), ptr_file)){
    (ptr_vector)[i] = atoi(buffer);
    i++;
  }
  fclose(ptr_file);
}

int multiply(int * ptr_vector1, int * ptr_vector2, int size){
  int result = 0;
  for(int i = 0; i < size; i++){
    result += ptr_vector1[i] * ptr_vector2[i];
  }
  return result;
}
